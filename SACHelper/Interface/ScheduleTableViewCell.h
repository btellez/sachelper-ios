//
//  ScheduleTableViewCell.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/11/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;

@end
