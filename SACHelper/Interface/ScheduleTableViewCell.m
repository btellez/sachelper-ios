//
//  ScheduleTableViewCell.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/11/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "ScheduleTableViewCell.h"

@implementation ScheduleTableViewCell
@synthesize dateLabel = _dateLabel;
@synthesize detailTextLabel = _detailTextLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
