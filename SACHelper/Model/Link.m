//
//  Link.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/10/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "Link.h"

@implementation Link : NSObject

-(id) init
{
    self = [super init];
    if (self) {
        self.title = @"Default Link";
        self.desc = @"Default Description";
        self.url = @"http://www.google.com";
    }
    return self;
}

- (id) initWithURL: (NSString *) url
             title: (NSString *) title
       description: (NSString *) desc
{
    self = [super init];
    if (self) {
        self.title = title;
        self.desc  = desc;
        self.url   = url;
    }
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"LINK[title: '%@', url:'%@', desc:'%@']", self.title, self.url, self.desc];
}

@end
