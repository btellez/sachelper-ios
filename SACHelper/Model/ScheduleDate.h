//
//  ScheduleDate.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/11/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScheduleDate : NSObject
@property (strong, nonatomic) NSString * desc;
@property (strong, nonatomic) NSString * date;
@property (strong, nonatomic) NSString * type; // TODO: Make it an enum-type

- (id) initWithDate: (NSString *) date
        description: (NSString *) desc
               type: (NSString *) type;
@end
