//
//  ScheduleDate.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/11/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "ScheduleDate.h"

@implementation ScheduleDate

- (id)init
{
    self = [super init];
    if (self) {
        self.date = @"Undefined Date";
        self.desc = @"Undefined Desc";
        self.type = @"Undefined Type";
    }
    return self;
}

- (id) initWithDate: (NSString *) date
        description: (NSString *) desc
               type: (NSString *) type
{
    self = [super init];
    if (self) {
        self.date = date;
        self.desc = desc;
        self.type = type;
    }
    return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"ScheduleDate[date: '%@', desc:'%@', type:'%@']", self.date, self.desc, self.type];
}
@end
