//
//  DirectoryEntry.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/11/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "DirectoryEntry.h"

@implementation DirectoryEntry

-(NSString *)description
{
    return [NSString stringWithFormat:
            @"DirectoryEntry[title: '%@',  description: '%@',  hours: '%@',  telephone: '%@',  email: '%@',  url: '%@']",
            self.title,
            self.desc,
            self.hours,
            self.telephone,
            self.email,
            self.location];
}
@end
