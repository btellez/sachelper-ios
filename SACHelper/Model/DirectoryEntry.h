//
//  DirectoryEntry.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/11/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DirectoryEntry : NSObject
@property (strong, nonatomic) NSString * title;
@property (strong, nonatomic) NSString * desc;
@property (strong, nonatomic) NSString * hours;
@property (strong, nonatomic) NSString * telephone;
@property (strong, nonatomic) NSString * email;
@property (strong, nonatomic) NSString * location;
@end
