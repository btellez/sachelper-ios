//
//  Link.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/10/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Link : NSObject
@property (strong, nonatomic) NSString * url;
@property (strong, nonatomic) NSString * title;
@property (strong, nonatomic) NSString * desc;
@end
