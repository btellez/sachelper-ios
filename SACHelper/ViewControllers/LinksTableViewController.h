//
//  LinksViewController.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/10/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LinksTableViewController : UITableViewController
@property (strong, nonatomic) NSArray * links;
@end
