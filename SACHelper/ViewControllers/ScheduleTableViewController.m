//
//  ScheduleViewController.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/10/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "ScheduleTableViewController.h"
#import "ScheduleDate.h"
#import "ScheduleTableViewCell.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation ScheduleTableViewController
{
    void (^onSuccess)(AFHTTPRequestOperation *, id);
    void (^onFailure)(AFHTTPRequestOperation *, NSError *);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Schedule";
        self.dates = @[];
        [self loadNetworkData];
       
    }
    return self;
}


- (void) setUpNetworkCallbacks
{
    __weak typeof(self) welf = self;
    //Network Success Callback:
    onSuccess = ^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableArray * temp = [[NSMutableArray alloc] init];
        NSLog(@"Done!: %@", responseObject);
        for (id entry in responseObject) {
            [temp addObject:[[ScheduleDate alloc] initWithDate:entry[@"date"] description:entry[@"desc"] type:@""]];
        }
        [welf setDates: [[NSArray alloc] initWithArray:temp]];
        [welf.tableView reloadData];
    };
    
    // Network Failure callback:
    onFailure = ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    };
}


- (void) loadNetworkData
{
    [self setUpNetworkCallbacks];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:URL_SCHEDULE
      parameters:nil
         success: onSuccess
         failure: onFailure
     ];
}


- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.dates.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ScheduleCell";
    ScheduleTableViewCell *cell = (ScheduleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ScheduleTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.dateLabel.text = [[self.dates[indexPath.row] date] description];
    cell.descriptionLabel.text = [self.dates[indexPath.row] desc];
    return cell;
}



-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Row Touched: %ld", (long)indexPath.row);
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
@end
