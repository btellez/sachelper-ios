//
//  LinksViewController.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/10/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "LinksTableViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation LinksTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Links";
        self.links = @[];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET: URL_LINKS
          parameters:nil
             success: ^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSMutableArray * temp = [[NSMutableArray alloc] init];
                 NSLog(@"Done!: %@", responseObject);
                 for (id entry in responseObject) {
                     [temp addObject:entry[@"name"]];
                 }
                 [self setLinks:[[NSArray alloc] initWithArray:temp]];
                 [self.tableView reloadData];
             }
         
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"Error: %@", error);
             }];
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.links.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.textLabel.text = self.links[indexPath.row];
    return cell;
}

@end
