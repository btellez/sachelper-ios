//
//  DirectoryViewController.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/10/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "DirectoryTableViewController.h"
#import "DirectoryDetailsViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@implementation DirectoryTableViewController
{
    void (^onSuccess)(AFHTTPRequestOperation *, id);
    void (^onFailure)(AFHTTPRequestOperation *, NSError *);
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Directory";
        //        self.tabBarItem.image = [UIImage imageNamed:@"tab_icon_feed.png"];
        self.places = @[];
        [self loadNetworkData];
    }
    return self;
}

    
- (void) setUpNetworkCallbacks
{
    __weak typeof(self) welf = self;
    
    onSuccess =^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableArray * temp = [[NSMutableArray alloc] init];
        NSLog(@"Done!: %@", responseObject);
        for (id entry in responseObject) {
            DirectoryEntry * e = [[DirectoryEntry alloc] init];
            [e setTitle:entry[@"name"]];
            [e setLocation:entry[@"location"]];
            [e setEmail:entry[@"email"]];
            [e setTelephone:entry[@"phone"]];
            [e setDesc:entry[@"description"]];
            [e setHours:entry[@"hours"]];
            
            [temp addObject:e];
        }
        [welf setPlaces:[[NSArray alloc] initWithArray:temp]];
        [welf.tableView reloadData];
    };
    
    onFailure = ^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    };
}


- (void) loadNetworkData
{
    [self setUpNetworkCallbacks];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET: URL_DIRECTORY
      parameters: nil
         success: onSuccess
         failure: onFailure
     ];
}



- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.places.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    cell.textLabel.text = [self.places[indexPath.row] title];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Row Selected!!!");
    NSString * place = [self.places objectAtIndex:indexPath.row];
    DirectoryDetailsViewController * directoryDetails = [[DirectoryDetailsViewController alloc] init];
    [directoryDetails setName:place];
    [directoryDetails setData:self.places[indexPath.row]];
    [self.navigationController pushViewController:directoryDetails animated:true];
}

@end
