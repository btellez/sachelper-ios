//
//  MapViewController.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/10/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapTableViewController : UITableViewController
@property (strong, nonatomic) NSArray * mapImages;
@end
