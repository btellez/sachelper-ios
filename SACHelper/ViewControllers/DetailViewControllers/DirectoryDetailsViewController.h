//
//  DirectoryDetailsViewController.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/13/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DirectoryEntry.h"
@interface DirectoryDetailsViewController : UIViewController
@property (strong, nonatomic) NSString * name;
@property (strong, nonatomic) DirectoryEntry * data;
@end
