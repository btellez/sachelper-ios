//
//  DirectoryDetailsViewController.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/13/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "DirectoryDetailsViewController.h"
#import "DirectoryEntryView.h"

@interface DirectoryDetailsViewController ()

@end

@implementation DirectoryDetailsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = _data.title;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (_data) {
        DirectoryEntryView * v = (DirectoryEntryView*) self.view;
        v.name.text = _data.title;
        v.desc.text = _data.desc;
        v.telephone.text = _data.telephone;
        v.hours.text = _data.hours;
        v.email.text = _data.email;
        v.location.text = _data.location;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
