//
//  DirectoryEntryView.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/13/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectoryEntryView : UIView
@property (strong, nonatomic) IBOutlet UILabel * name;
@property (strong, nonatomic) IBOutlet UILabel * desc;
@property (strong, nonatomic) IBOutlet UILabel * hours;
@property (strong, nonatomic) IBOutlet UILabel * telephone;
@property (strong, nonatomic) IBOutlet UILabel * email;
@property (strong, nonatomic) IBOutlet UILabel * location;
@end
