//
//  DirectoryEntryView.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/13/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "DirectoryEntryView.h"

@implementation DirectoryEntryView
@synthesize name = _name;
@synthesize desc = _desc;
@synthesize hours = _hours;
@synthesize telephone = _telephone;
@synthesize email = _email;
@synthesize location = _location;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
