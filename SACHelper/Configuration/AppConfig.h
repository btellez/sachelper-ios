//
//  AppConfig.h
//  SACHelper
//
//  Created by Bladymir Tellez on 8/12/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * URL_SCHEDULE;
extern NSString * URL_DIRECTORY;
extern NSString * URL_LINKS;
extern NSString * URL_MAP;
