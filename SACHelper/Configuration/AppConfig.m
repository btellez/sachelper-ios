//
//  AppConfig.m
//  SACHelper
//
//  Created by Bladymir Tellez on 8/12/14.
//  Copyright (c) 2014 Computer Science IO. All rights reserved.
//

#import "AppConfig.h"

NSString * URL_SCHEDULE = @"http://sachelper.herokuapp.com/schedule";
NSString * URL_DIRECTORY = @"http://sachelper.herokuapp.com/directory";
NSString * URL_LINKS = @"http://sachelper.herokuapp.com/links";
NSString * URL_MAP = @"http://sachelper.herokuapp.com/maps";